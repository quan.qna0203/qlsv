// var sinhvien = {
//   maSinhVien: "",
//   tenSinhVien: "",
//   loaiSinhVien: 1,
//   diemToan: 0,
//   diemVan: 0,
//   tinhDiemTB: function () {
//     return (Number(this.diemToan) + Number(this.diemVan)) / 2;
//   },
//   xepLoaiSinhVien: function () {
//     var output = "Đậu";
//     if (this.tinhDiemTB < 5) {
//       output = "Rớt";
//     }
//     return output;
//   },
// };

var sinhvien = new SinhVien();
document.querySelector("#submit").onclick = function (e) {
  e.preventDefault();
  sinhvien.maSinhVien = document.querySelector("#maSinhVien").value;
  sinhvien.tenSinhVien = document.querySelector("#tenSinhVien").value;
  sinhvien.loaiSinhVien = document.querySelector("#loaiSinhVien").value;
  sinhvien.diemToan = document.querySelector("#diemToan").value;
  sinhvien.diemVan = document.querySelector("#diemVan").value;

  document.querySelector("#output-tenSinhVien").innerHTML =
    sinhvien.tenSinhVien;
  document.querySelector("#output-maSinhVien").innerHTML = sinhvien.maSinhVien;
  if (sinhvien.loaiSinhVien === "1") {
    document.querySelector("#output-loaiSinhVien").innerHTML = "Bình thường";
  }else{
    document.querySelector("#output-loaiSinhVien").innerHTML = "Khó Khăn";
  }
  document.querySelector("#output-diemTB").innerHTML = sinhvien.tinhDiemTB();
  document.querySelector("#output-xepLoaiSinhVien").innerHTML =
    sinhvien.xepLoaiSinhVien();
  document.querySelector("#output-tenSinhVien").innerHTML = sinhvien.maSinhVien;
};
