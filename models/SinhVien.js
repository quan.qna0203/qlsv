// prototype (VN kêu là lớp đối tượng) dùng để tạo ra format cho đối tượng
function SinhVien() {
  // Thuoc tinh
  this.maSinhVien = "";
  this.tenSinhVien = "";
  this.loaiSinhVien = 1;
  this.diemToan = 0;
  this.diemVan = 0;
  this.tinhDiemTB = function () {
    return (Number(this.diemToan) + Number(this.diemVan)) / 2;
  };
  this.xepLoaiSinhVien = function () {
    var output = "Đậu";
    if (this.tinhDiemTB < 5) {
      output = "Rớt";
    }
    return output;
  };
}
